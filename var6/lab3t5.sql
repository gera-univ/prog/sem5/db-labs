\echo Задание 3.5.2. Простой CASE

select case name
when 'Dummy Books' then 'Тестовые Книги'
when 'Other Books' then 'Другие Книги'
else name end from genre;

\echo Задание 3.5.3. Поисковой CASE

select isbn, (case
when extract('day' from date_loaned) < 15 then 'First half of '
else 'Second half of ' end || to_char(date_loaned, 'MonthYYYY')) as date_loaned
from loan;

\echo Задание 3.5.4. WITH

with person_average_hold_time as (
select person_id, avg(date_returned - date_loaned) from person inner join loan using(person_id) 
group by person_id) select * from person inner join person_average_hold_time using(person_id);

\echo Задание 3.5.8. NULLIF

select isbn, coalesce(nullif(title, 'Test Book'), 'Тестовая Книга') as title from book;

\echo Задание 3.5.11. ROLLUP

select count(isbn), coalesce(language, 'Total') from book
group by rollup(language);
