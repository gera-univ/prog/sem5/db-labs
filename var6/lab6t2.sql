drop routine if exists change_genre;
drop trigger if exists loan_validate on loan;
drop routine if exists loan_validate;

-- Изменение жанра книги (а значит и католога) по ключевым словам
create function change_genre(keywords varchar, new_genre integer)
returns integer as $$
	with moved_books as (
		update book set genre_id=new_genre
		where genre_id != new_genre
		and string_to_array(book.keywords, ' ') @> string_to_array(keywords, ' ')
		returning *) select count(*) as moved from moved_books;
$$ language sql;

-- Валидация таблицы loan
create function loan_validate() returns trigger as $$
begin
	if new.date_loaned > new.date_due then
		raise exception 'date_loaned cannot be greater than date_due';
	end if;
	if new.date_loaned > new.date_returned then
		raise exception 'date_loaned cannot be greater than date_returned';
	end if;
	return new;
end;
$$ language plpgsql;

create trigger loan_validate before insert or update on loan
for each row execute function loan_validate();
