drop view if exists view_book_title;
drop view if exists view_book_author;
drop view if exists view_book_publisher;
drop view if exists view_book_keyword;
drop view if exists view_book_keyword_part;
drop view if exists view_book_count;
drop view if exists view_books_due;
drop table if exists loan;
drop table if exists person;
drop table if exists authorship;
drop table if exists author;
drop table if exists book;
drop table if exists publisher;
drop table if exists genre;
drop table if exists catalogue;

create table catalogue (
	name           varchar(255),
	catalogue_id   integer not null primary key
);

create table genre (
	name           varchar(255),
	genre_id       integer not null primary key,
	catalogue_id   integer not null references catalogue (catalogue_id)
);

create table publisher (
	name           varchar(255),
	country        varchar(255),
	publisher_id   integer not null primary key
);

create table book (
	isbn           integer not null primary key,
	title          varchar(255),
	year_published integer,
	language       varchar(255),
	keywords       varchar(1024),
	genre_id       integer not null references genre (genre_id),
	publisher_id   integer not null references publisher (publisher_id)
);

create table author (
	name           varchar(255),
	surname        varchar(255),
	date_birth     date,
	date_death     date,
	author_id      integer not null primary key
);

create table authorship (
	isbn           integer not null references book (isbn),
	author_id      integer not null references author (author_id),

	primary key (isbn, author_id)
);

create table person (
	name           varchar(255),
	surname        varchar(255),
	phone_number   varchar(255),
	person_id      integer not null primary key
);

create table loan (
	isbn           integer not null references book (isbn),
	person_id      integer not null references person (person_id),
	date_loaned    date,
	date_due       date,
	date_returned  date,

	primary key (isbn, person_id)
);

insert into catalogue (catalogue_id, name) values (10, 'Miscellaneous Literature');
insert into genre (genre_id, catalogue_id, name) values (10, 10, 'Dummy Books');
insert into publisher (publisher_id, name, country) values (10, 'Test Publisher', 'USA');
insert into book 
(isbn, title, year_published, language, keywords, genre_id, publisher_id) 
values (10, 'Test Book', 2020, 'English', 'Test', 10, 10);
insert into author
(author_id, name, surname, date_birth, date_death)
values (10, 'Test', 'Testovich', date '10-02-1952', date '10-10-2020');
insert into authorship (isbn, author_id)
values (10, 10);
insert into person
(person_id, name, surname, phone_number)
values (10, 'John', 'Smith', '+1111111111');
insert into loan
(isbn, person_id, date_loaned, date_due, date_returned)
values (10, 10, date '12-01-2021', date '12-31-2021', date '12-10-2021');
insert into catalogue (catalogue_id, name) values (20, 'Other Catalogue');
insert into genre (genre_id, catalogue_id, name) values (20, 20, 'Other Books');
