\echo Задание 3.1.1. Выдать информацию о местоположении отдела продаж (SALES) компании.
select * from dept where deptname = 'SALES';
\echo Задание 3.1.2. Выдать информацию об отделах, расположенных в Chicago и New York.
select * from dept where deptaddr = 'CHICAGO' or deptaddr = 'NEW YORK';
\echo Задание 3.1.3. Найти минимальную заработную плату, начисленную в 2009 году.
select min(salvalue) from salary where year = '2009';
\echo Задание 3.1.4. Выдать информацию обо всех работниках, родившихся не позднее 1 января 1960 года.
select * from emp where birthdate <= date '1960-01-01';
\echo Задание 3.1.5. Подсчитать число работников, сведения о которых имеются в базе данных .
select count(*) from emp;
\echo Задание 3.1.6. Найти работников, чьё имя состоит из одного слова. Имена выдать на нижнем регистре, с удалением стоящей справа буквы t.
select lower(regexp_replace(empname, '.T', 'T')) from emp where empname not like '% %';
\echo Задание 3.1.7. Выдать информацию о работниках, указав дату рождения в формате день(число), месяц(название), год(название). Тоже, но год числом.
select empname, to_char(birthdate, 'DD Month YYYY') from emp;
\echo Задание 3.1.8. Выдать информацию о должностях, изменив названия должности “CLERK” и “DRIVER” на “WORKER”.
select replace(replace(jobname, 'DRIVER', 'WORKER'), 'CLERK', 'WORKER') from job;
\echo Задание 3.1.9. Определите среднюю зарплату за годы, в которые были начисления не менее чем за три месяца. (Ипользовать HAVING)
select year, avg(salvalue) from salary group by year having count(distinct month) >= 3;
\echo Задание 3.1.10. Выведете ведомость получения зарплаты с указанием имен служащих.
select emp.empno, empname, month, year, salvalue from emp inner join salary on emp.empno = salary.empno;
\echo Задание 3.1.11. Укажите сведения о начислении сотрудникам зарплаты, попадающей в вилку: минимальный оклад по должности - минимальный оклад по должности плюс пятьсот. Укажите соответствующую вилке должность.
select empname, jobname, salvalue, minsalary from emp inner join career on emp.empno = career.empno inner join job on job.jobno = career.jobno inner join salary on emp.empno = salary.empno where salvalue > minsalary and salvalue < minsalary + 500;
\echo Задание 3.1.12. Укажите сведения о заработной плате, совпадающей с минимальными окладами по должностям (с указанием этих должностей). (Использовать "equi" join)
select * from salary join emp on salary.empno = emp.empno join career on emp.empno = career.empno join job on career.jobno = job.jobno where salary.salvalue = job.minsalary;
\echo Задание 3.1.13. Найдите сведения о карьере сотрудников с указанием вместо номера сотрудника его имени. (NATURAL JOIN)
select * from emp natural join career where empname = 'ALLEN';
\echo Задание 3.1.14. Найдите сведения о карьере сотрудников с указанием вместо номера сотрудника его имени. (INNER JOIN)
select * from emp inner join career on emp.empno = career.empno where empname = 'ALLEN';
\echo Задание 3.1.15. Выдайте сведения о карьере сотрудников с указанием их имён, наименования должности, и названия отдела.
select empname, deptname, jobname from emp full outer join career using(empno) full outer join job using(jobno) full outer join dept using(deptno);
\echo Задание 3.1.16. Выдайте сведения о карьере сотрудников с указанием их имён. Выдайте сведения о карьере сотрудников с указанием их имён. (LEFT OUTER JOIN)
select * from emp right outer join career using(empno) right outer join job using(jobno) where empname = 'ALLEN';
\echo Задание 3.1.16. Выдайте сведения о карьере сотрудников с указанием их имён. Выдайте сведения о карьере сотрудников с указанием их имён. (RIGHT OUTER JOIN)
select * from emp left outer join career using(empno) left outer join job using(jobno) where empname = 'ALLEN';
\echo Задание 3.1.16. Выдайте сведения о карьере сотрудников с указанием их имён. Выдайте сведения о карьере сотрудников с указанием их имён. (FULL OUTER JOIN)
select * from emp full outer join career using(empno) full outer join job using(jobno) where empname = 'ALLEN';
