-- Рефлексивное соединение. Представление отношений родитель-потомок.
\echo Задание 3.3.1. Требуется представить имя каждого сотрудника таблицы EMP а также имя его руководителя.
select format('%s works for %s', subordinate.empname, manager.empname) from emp as subordinate join emp as manager on manager.empno = subordinate.manager_id;

-- Иерархический запрос
\echo Задание 3.3.2. Требуется представить имя каждого сотрудника таблицы EMP (даже сотрудника, которому не назначен руководитель) и имя его руководителя.
with hierarchy(empname, empno, manager_id, manager_name) as (
	select empname, empno, manager_id, '' from emp where manager_id is NULL
	union all
	select s.empname, s.empno, s.manager_id, m.empname from emp as s join emp as m on m.empno = s.manager_id
) select format('%s reports to %s', empname, manager_name) as hierarchy from hierarchy;

-- Представление отношений потомок-родитель-прародитель
\echo Задание 3.3.3. Требуется показать иерархию от CLARK до JOHN KLINTON.
with recursive hierarchy(empname, empno, manager_id) as (
	select empname, empno, manager_id from emp where empname='CLARK'
	union
	select m.empname, m.empno, m.manager_id from hierarchy s, emp m
	where s.manager_id = m.empno
) select string_agg(empname, '->') as hierarchy from hierarchy;

-- Иерархическое представление таблицы
\echo Задание 3.3.4. Требуется получить результирующее множество, описывающее иерархию всей таблицы.
with recursive hierarchy(hstring, empname, empno, manager_id) as (
	select 'JOHN KLINTON', empname, empno, manager_id from emp where empname='JOHN KLINTON'
	union
	select m.hstring || '->' || s.empname, s.empname, s.empno, s.manager_id from hierarchy m, emp s
	where s.manager_id = m.empno
) select hstring as hierarchy from hierarchy;

-- Представление уровня иерархии
\echo Задание 3.3.5. Требуется показать уровень иерархии каждого сотрудника.
with recursive hierarchy(hstring, l, empname, empno, manager_id, path) as (
	select 'JOHN KLINTON', 1, s.empname, s.empno, s.manager_id, cast (array[s.empno] as numeric[])
	from emp s where s.empname='JOHN KLINTON'
	union
	select lpad(s.empname, length(s.empname)+l*4, '    '), m.l+1, s.empname, s.empno, s.manager_id, m.path || s.empno
	from hierarchy m, emp s
	where s.manager_id = m.empno
) select hstring as hierarchy, path from hierarchy order by path;

-- Выбор всех дочерних строк для заданной строки
\echo Задание 3.3.6. Требуется найти всех служащих, которые явно или неявно подчиняются ALLEN.
with recursive hierarchy(empname, empno, manager_id) as (
	select empname, empno, manager_id from emp where empname='ALLEN'
	union
	select s.empname, s.empno, s.manager_id from hierarchy m, emp s
	where s.manager_id = m.empno
) select empname as hierarchy from hierarchy;
