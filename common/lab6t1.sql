alter table salary drop column if exists tax;
alter table salary add column tax numeric;

create or replace procedure calculate_tax() as $$
declare
	salary_view record;
	employee_view record;
	year_salary_view record;
	coeff numeric;
begin
	for employee_view in select empno from emp
	loop
		for year_salary_view in
		select year, sum(salvalue)
		from salary where empno=employee_view.empno
		group by year
		loop
			if year_salary_view.sum < 20000 then
				coeff = 0.9;
			elsif year_salary_view.sum between 20000 and 30000 then
				coeff = 0.12;
			else
				coeff = 0.15;
			end if;
			raise notice 'employee %, salary %, year %, coeff %',
			employee_view.empno, year_salary_view.sum,
			year_salary_view.year, coeff;
			update salary set tax = salvalue * coeff
			where empno = employee_view.empno
			and year = year_salary_view.year;
		end loop;
	end loop;
end
$$ language plpgsql;
